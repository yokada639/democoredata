//
//  User+CoreDataProperties.swift
//  CoreDataProject
//
//  Created by Murphy on 2024/4/29.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var name: String?
    @NSManaged public var age: Int32
    @NSManaged public var birthday: String?

}

extension User : Identifiable {

}
