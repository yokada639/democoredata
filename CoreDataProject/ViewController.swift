//
//  ViewController.swift
//  CoreDataProject
//
//  Created by Murphy on 2024/4/27.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    //用來操作Core Data的常數
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //(Read)查詢回傳的list
    var originDataList:[User]?

    //(Create)Data ObjectID
    var dataID1: NSManagedObjectID?
    var dataID2: NSManagedObjectID?
    var dataID3: NSManagedObjectID?
    override func viewDidLoad() {
        super.viewDidLoad()
        self.deleteAll()
        self.queryData()
        
        //(Create)新增資料
        self.createNewData()
       
        //(Read)查詢整個Entity
        self.queryData()

        //(Read)查詢Entity內指定資料
        self.queryData(objectID: dataID1)
   
        //(Read)利用Predicate查詢指定資料
        self.queryDataWithPredicate()
        
        //(Update)更新Entity內指定資料
        self.updateData(objectID: dataID1)

        //(Delete)刪除Entity內指定資料
        self.deleteData(objectID: dataID1)
    }
    
    // MARK: - 新增資料進core data
    func createNewData(){
        //第一種建立Entity實體的方式
        let newData1 = User(context: context)
        newData1.name = "張三"
        newData1.age = 30
        newData1.birthday = "1994/1/1"
        //儲存進core data
        self.appDelegate.saveContext()
        //把Object ID存起來
        dataID1 = newData1.objectID
        
        let newData2 = User(context: context)
        newData2.name = "李四"
        newData2.age = 40
        newData2.birthday = "1984/1/1"
        self.appDelegate.saveContext()
        dataID2 = newData2.objectID
        
        //第二種建立Entity實體的方式
        let newData3 = NSEntityDescription.insertNewObject(forEntityName: "User", into: context ) as? User
        newData3?.name = "王五"
        newData3?.age = 50
        newData3?.birthday = "1974/1/1"
        self.appDelegate.saveContext()
        dataID3 = newData3?.objectID
        
        //以下是為了測試所寫的內容
        queryData()
        if let list = originDataList, originDataList?.count != 0{
            for obj in list{
                print("資料新增成功，新增的使用者姓名是\(String(describing: (obj.name)!))，年齡是\(String(describing: obj.age))歲，生日是\(String(describing: (obj.birthday)!))\r\n")
            }
        }
    }
    
    // MARK: - 取得core data資料(Object ID / predicate / 全部)
    func queryData(objectID: NSManagedObjectID? = nil){
        if let objID = objectID{
            //透過objectID抓到core data裡的物件，再轉型成Entity的Class
            do {
                let object = try context.existingObject(with: objID) as? User
                print("執行queryData(objectID)方法\r\n查詢的使用者姓名是\(String(describing: (object?.name)!))，年齡是\(String(describing: (object?.age)!))歲，生日是\(String(describing: (object?.birthday)!))\r\n")
            } catch {
                print("無法找到相應的 NSManagedObject\r\n")
                print("Error:\(error)\r\n")
            }
        }
        else{
            //取得User Entity全部資料
            do {
                let fetchRequest : NSFetchRequest<User> = User.fetchRequest()
                  self.originDataList = try context.fetch(fetchRequest)
                print("執行queryData()方法\r\n查詢User Entity資料成功，資料筆數有\(String(describing: (originDataList?.count)!))筆\r\n")
                
                //以下是為了測試所寫的內容
                if let list = originDataList, originDataList?.count != 0{
                    for obj in list{
                        print("使用者姓名是\(String(describing: (obj.name)!))，年齡是\(String(describing: obj.age))歲，生日是\(String(describing: (obj.birthday)!))\r\n")
                    }
                }
            }catch{
                print("查詢core data資料失敗\r\n")
                fatalError("Failed to fetch data: \(error)\r\n")
            }
        }
    }
    
    func queryDataWithPredicate(){
        do {
            let fetchRequest : NSFetchRequest<User> = User.fetchRequest()
            fetchRequest.predicate = NSPredicate(format: "age >= %d", 35)
            let result = try context.fetch(fetchRequest)
            for user in result{
                print("使用predicate查詢方法：年齡大於35歲的使用者有：\(String(describing: (user.name)!))，年齡是\(String(describing: user.age))歲，生日是\(String(describing: (user.birthday)!))\r\n")
            }
        }catch{
            fatalError("Failed to fetch data: \(error)")
        }
    }
    
    // MARK: - 更新資料
    func updateData(objectID: NSManagedObjectID?){
        do{
            //透過objectID抓到core data裡的物件，再轉型成Entity的Class
            if let objID = objectID{
                let object = try context.existingObject(with: objID) as! User
                print("更新前：使用者的姓名是\(String(describing: (object.name)!))，年齡是\(object.age)歲，生日是\(String(describing: (object.birthday)!))\r\n")
                object.name = "張三豐"
                self.appDelegate.saveContext()
                print("更新後：使用者的姓名是\(String(describing: (object.name)!))，年齡是\(object.age)歲，生日是\(String(describing: (object.birthday)!))\r\n")
            }
        }
        catch{
            print(error)
        }
    }
    
    // MARK: - 刪除資料
    func deleteData(objectID: NSManagedObjectID? = nil){
        do{
            //透過objectID抓到core data裡的物件，再轉型成Entity的Class
            if let objID = objectID{
                self.queryData()
                if let list = originDataList, originDataList?.count != 0{
                    for obj in list{
                        print("資料刪除前：使用者姓名是\(String(describing: (obj.name)!))，年齡是\(String(describing: obj.age))歲，生日是\(String(describing: (obj.birthday)!))\r\n")
                    }
                }
                let object = try context.existingObject(with: objID) as! User
                print("準備刪除的資料：使用者姓名是\(String(describing: (object.name)!))，年齡是\(String(describing: object.age))歲，生日是\(String(describing: (object.birthday)!))\r\n")
                self.context.delete(object)
                self.appDelegate.saveContext()
                print("刪除成功\r\n")
                self.queryData()
                if let list = originDataList, originDataList?.count != 0{
                    for obj in list{
                        print("資料刪除後：使用者姓名是\(String(describing: (obj.name)!))，年齡是\(String(describing: obj.age))歲，生日是\(String(describing: (obj.birthday)!))\r\n")
                    }
                }
            }
            else{
                print("無此Object ID，無法刪除。\r\n")
            }
        }
        catch{
            print(error)
        }
    }
    
    //Entity內所有資料刪除
    func deleteAll(){
        do {
            let fetchRequest : NSFetchRequest<User> = User.fetchRequest()
            self.originDataList = try context.fetch(fetchRequest)
            if let list = originDataList, list.count != 0{
                for object in list {
                    context.delete(object)
                }
                self.appDelegate.saveContext()
            }
        }catch{
            print("刪除core data資料失敗\r\n")
        }
    }
}

